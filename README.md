# nodejs-generator-code

[![Yeoman Icon](./assets/yeoman.png)](./assets/yeoman.png)[![VSCode Icon](./assets/vscode.png)](./assets/vscode.png)

Arch(<sub>/Manjaro/Endeavour/etc...</sub>) Linux `PKGBUILD` for `generator-code` via `npm`.

## Description

`generator-code` - Yeoman code generator for VSCode Extensions.

## Installation

First, build the package.  Then install it:
    
    pkgver=$(grep pkgver PKGBUILD | cut -d= -f2)
    pkgrel=$(grep pkgrel PKGBUILD | cut -d= -f2)
    
    pacman -U nodejs-generator-code-$pkgver-$pkgrel-any.pkg.tar.xz

## Build

    makepkg --cleanbuild --sign

## Verify

    pkgver=$(grep pkgver PKGBUILD | cut -d= -f2)
    pkgrel=$(grep pkgrel PKGBUILD | cut -d= -f2)
    
    gpg --verify nodejs-generator-code-$pkgver-$pkgrel-any.pkg.tar.xz.sig

## Dependencies

This plugin depends on Yeoman via AUR: [`aur/yo`][0].

### Development Dependencies

- `ttf-jetbrains-mono-nerd`: For generating the `assets/*.png` files.
- `jq`: Development dependency of Yeoman / `yo`.
- `npm`: For `npm install` & repackaging the nodejs module.

### Build Logos

    cd assets && make

## Usage

See upstream [docs][1].

## Support

For support with the actual software, please reach out to the [upstream project][2]

For support with this `PKGBUILD` for packaging purposes only, feel free to interact via this project repo.

## License

The `PKGBUILD` and package maintainer files here are licensed under [GPLv3][3].

[0]: https://aur.archlinux.org/packages/yo
[1]: https://www.npmjs.com/package/generator-code
[2]: https://github.com/microsoft/vscode-generator-code/
[3]: https://choosealicense.com/licenses/gpl-3.0/
